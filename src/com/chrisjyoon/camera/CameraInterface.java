package com.chrisjyoon.camera;

import android.graphics.Bitmap;

public interface CameraInterface {
	public void saveImage(Bitmap bitmap, boolean upload, String fileName);
	public void cameraFail();
}

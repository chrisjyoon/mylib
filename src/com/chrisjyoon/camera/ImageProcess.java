package com.chrisjyoon.camera;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;

import com.chrisjyoon.common.Logger;

public class ImageProcess {
	byte[] mData;
	
	public ImageProcess(byte[] data) {
		mData = data;
	}
	
	public Bitmap rotateImage(int degree) {
		Bitmap original = BitmapFactory.decodeByteArray(mData, 0, mData.length);
		
		Matrix matrix = null;
		Bitmap rotate = original;

		if (degree != 0) {
	          matrix = rotate((matrix == null ? new Matrix() : matrix), degree);
	          
	          rotate = Bitmap.createBitmap(original, 0, 0, 
	        		  original.getWidth(), original.getHeight(), matrix, true);
	          
	          original.recycle();
		}
		return rotate;
	}
	
	private Matrix rotate(Matrix input, int degree) {
	    input.setRotate(degree);

	    return input;
	}
	
	/**
	 * 비율 유지하면서 스케일 후 필요시 크롭
	 * @param org 원본 이미지
	 * @param w 스케일할 새 너비
	 * @param h 스케일할 새 높이
	 * @return 스케일 및 크롭된 새 이미지
	 */
	public static Bitmap scaleImageByWidth(Bitmap org, int w, int h) {
		if (w == org.getWidth() && h == org.getHeight()) {
			return org;
		}
		
		double ratio = (double) org.getWidth() / org.getHeight();
        int newHeight = (int) (w / ratio);
        
        Bitmap scaled = Bitmap.createScaledBitmap(org, w, newHeight, false);
        
        org.recycle();
        
        int y = 0;
        if (newHeight > h) {
        	y = (newHeight - h) / 2;
        	Bitmap cropped = Bitmap.createBitmap(scaled, 0, y, w, h);
        	scaled.recycle();
        	return cropped;
        }
		
		return scaled;
	}
}
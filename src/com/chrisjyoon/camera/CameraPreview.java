package com.chrisjyoon.camera;

import android.content.Context;
import android.graphics.Bitmap;
import android.hardware.Camera;
import android.hardware.Camera.AutoFocusCallback;
import android.hardware.Camera.ShutterCallback;
import android.hardware.Camera.Size;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;

import com.chrisjyoon.common.Logger;

import java.io.IOException;
import java.util.List;

class CameraPreview extends ViewGroup implements SurfaceHolder.Callback,
	AutoFocusCallback {
    private final String TAG = "Preview";

    private SurfaceView mSurfaceView;
    private SurfaceHolder mHolder;
    private Size mPreviewSize;
    private int mPhotoWidth;
    private int mPhotoHeight;
    private int mPhotoCropWidth;
    private int mPhotoCropHeight;
    private List<Size> mSupportedPreviewSizes;
    private Camera mCamera;
    private CameraInterface mCameraCallback;
    private int mDisplayOrientation = 0;
    private boolean mIsPreview = false;
    private boolean mIsAutoFocusing = false;
    private Camera.Parameters mSavePreviewParams;

    @SuppressWarnings("deprecation")
    CameraPreview(Context context) {
        super(context);

        mSurfaceView = new SurfaceView(context);
        addView(mSurfaceView);
        
        // Install a SurfaceHolder.Callback so we get notified when the
        // underlying surface is created and destroyed.
        mHolder = mSurfaceView.getHolder();
        mHolder.addCallback(this);
        mHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
    }

    public void setCamera(Camera camera) {
        mCamera = camera;
        if (mCamera != null) {
            mSupportedPreviewSizes = mCamera.getParameters().getSupportedPreviewSizes();
            requestLayout();
        }
    }
    
    public void setDisplayOrientation(int displayOrientation) {
    	mDisplayOrientation = displayOrientation;
    	mCamera.setDisplayOrientation(displayOrientation);
    }

    public void switchCamera(Camera camera) {
       setCamera(camera);
       try {
           camera.setPreviewDisplay(mHolder);
       } catch (IOException exception) {
           Logger.e(TAG, "IOException caused by setPreviewDisplay()", exception);
       }
       Camera.Parameters parameters = camera.getParameters();
       parameters.setPreviewSize(mPreviewSize.width, mPreviewSize.height);
       requestLayout();

       camera.setParameters(parameters);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        // We purposely disregard child measurements because act as a
        // wrapper to a SurfaceView that centers the camera preview instead
        // of stretching it.
        final int width = resolveSize(getSuggestedMinimumWidth(), widthMeasureSpec);
        final int height = resolveSize(getSuggestedMinimumHeight(), heightMeasureSpec);
        setMeasuredDimension(width, height);

        if (mSupportedPreviewSizes != null) {
            mPreviewSize = getOptimalPreviewSize(mSupportedPreviewSizes, width, height);
        }
    }

    // thanks to CommonsWare
    // http://stackoverflow.com/questions/17126633/camera-setdisplayorientation-in-portrait-mode-breaks-aspect-ratio
    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        if (changed && getChildCount() > 0) {
            final View child = getChildAt(0);

            final int width = r - l;
            final int height = b - t;

            int previewWidth = width;
            int previewHeight = height;
            if (mPreviewSize != null) {
                if (mDisplayOrientation == 90
                        || mDisplayOrientation == 270) {
                	previewWidth = mPreviewSize.height;
                    previewHeight = mPreviewSize.width;
                } else {
	                previewWidth = mPreviewSize.width;
	                previewHeight = mPreviewSize.height;
                }
            }
            
            boolean useFirstStrategy = (width * previewHeight > height * previewWidth);
            boolean useFullBleed = true;//getHost().useFullBleedPreview();

            if ((useFirstStrategy && !useFullBleed)
                    || (!useFirstStrategy && useFullBleed)) {
            	final int scaledChildWidth = previewWidth * height / previewHeight;
            	child.layout((width - scaledChildWidth) / 2, 0,
                               (width + scaledChildWidth) / 2, height);
            } else {
            	final int scaledChildHeight = previewHeight * width / previewWidth;
            	child.layout(0, (height - scaledChildHeight) / 2, width,
            			(height + scaledChildHeight) / 2);
            }
        }
    }

    public void surfaceCreated(SurfaceHolder holder) {
        // The Surface has been created, acquire the camera and tell it where
        // to draw.
        try {
            if (mCamera != null) {
                mCamera.setPreviewDisplay(holder);

            }
        } catch (IOException exception) {
            Logger.e(TAG, "IOException caused by setPreviewDisplay()", exception);
        }
    }

    public void surfaceDestroyed(SurfaceHolder holder) {
        // Surface will be destroyed when we return, so stop the preview.
        if (mCamera != null) {
            mCamera.stopPreview();
        }
    }


    private Size getOptimalPreviewSize(List<Size> sizes, int w, int h) {
        final double ASPECT_TOLERANCE = 0.1;
        double targetRatio = (double) w / h;
        if (sizes == null) return null;

        Size optimalSize = null;
        double minDiff = Double.MAX_VALUE;
        int targetHeight = h;

        // Try to find an size match aspect ratio and size
        for (Size size : sizes) {
            double ratio = (double) size.width / size.height;
            if (Math.abs(ratio - targetRatio) > ASPECT_TOLERANCE) continue;
            if (Math.abs(size.height - targetHeight) < minDiff) {
                optimalSize = size;
                minDiff = Math.abs(size.height - targetHeight);
            }
        }

        // Cannot find the one match the aspect ratio, ignore the requirement
        if (optimalSize == null) {
            minDiff = Double.MAX_VALUE;
            for (Size size : sizes) {
                if (Math.abs(size.height - targetHeight) < minDiff) {
                    optimalSize = size;
                    minDiff = Math.abs(size.height - targetHeight);
                }
            }
        }
        return optimalSize;
    }

    public void surfaceChanged(SurfaceHolder holder, int format, int w, int h) {
    	if (mCamera == null) {
    		return;
    	}
        // Now that the size is known, set up the camera parameters and begin
        // the preview.
    	if (mIsPreview) {
    		mCamera.stopPreview();
    	}
        Camera.Parameters parameters = mCamera.getParameters();
        parameters.setPreviewSize(mPreviewSize.width, mPreviewSize.height);
        Logger.d("preview width = " + mPreviewSize.width + ", h = " + mPreviewSize.height);
        
        requestLayout();

        mCamera.setParameters(parameters);
        mCamera.startPreview();
        mIsPreview = true;
    }

    public void setPhotoSize(int w, int h) {
    	mPhotoWidth = w;
        mPhotoHeight = h;
    }
    
    public void setCropSize(int w, int h) {
    	mPhotoCropWidth = w;
    	mPhotoCropHeight = h;
    }
    
    public void autoFocus() {
        if (mIsPreview && !mIsAutoFocusing) {
        	String focusMode = mCamera.getParameters().getFocusMode();
        	if (focusMode.equals(Camera.Parameters.FOCUS_MODE_AUTO) ||
        			focusMode.equals(Camera.Parameters.FOCUS_MODE_MACRO)) {
	        	Logger.d("start autoFocus!");
	        	mCamera.autoFocus(this);
	        	mIsAutoFocusing = true;
        	}
        }
    }
    
	@Override
	public void onAutoFocus(boolean success, Camera camera) {
		mIsAutoFocusing = false;
	}
	
	public void takePicture() {
		if (mIsPreview) {
			mIsPreview = false;
			mSavePreviewParams = mCamera.getParameters();

//	        Camera.Parameters pictureParams = mCamera.getParameters();
//	        setPictureSize(pictureParams);

//            pictureParams.setPreviewSize(mPreviewSize.width, mPreviewSize.height);
//	        mCamera.setParameters(pictureParams);

            try {
            	mCamera.takePicture(new ShutterCallback() {
					@Override
					public void onShutter() {
					}
				}, null, new PictureCallback());
            }
            catch (Exception e) {
            	android.util.Log.e(getClass().getSimpleName(),
            			"Exception taking a picture", e);
            }
	    } else {
	    	throw new IllegalStateException("Preview mode must have started before you can take a picture");
	    }
	}
	
	public void setPictureSize(Camera.Parameters pictureParams) {
		if (mDisplayOrientation == 0) {
			pictureParams.setPictureSize(mPreviewSize.width, mPreviewSize.height);
		} else {
			int w = Math.max(mPreviewSize.height, mPhotoWidth);
			int h = Math.max(mPreviewSize.width, mPhotoHeight);
		}
	}
    
	private class PictureCallback implements Camera.PictureCallback {
		
		PictureCallback() {
		}
	
		@Override
		public void onPictureTaken(byte[] data, Camera camera) {
			mCamera.setParameters(mSavePreviewParams);
			
			if (data != null) {
				Logger.d("chris", "ready to write picture file");
				
				ImageProcess ip = new ImageProcess(data);
				Bitmap rotate = ip.rotateImage(mDisplayOrientation);

                Logger.d("rotate.getWidth() = " + rotate.getWidth() + ", mPhotoCropWidth = " + mPhotoCropWidth);
                Logger.d("rotate.getH = " + rotate.getHeight() + ", mPhotoCropH = " + mPhotoCropHeight);

                Bitmap scaled = ImageProcess.scaleImageByWidth(rotate, mPhotoCropWidth, mPhotoCropHeight);

//                int x = (rotate.getWidth() - mPhotoCropWidth) / 2;
//		        int y = (rotate.getHeight() - mPhotoCropHeight) / 2;
//
//		        Bitmap cropped = Bitmap.createBitmap(rotate, x, y, mPhotoCropWidth, mPhotoCropHeight);
//
		        mCameraCallback.saveImage(scaled, true, null);
				
				rotate.recycle();
		        scaled.recycle();
			}
			
			mCamera.startPreview();
			mIsPreview = true;
		}
	}
	
	public void setCameraCallback(CameraInterface callback) {
		mCameraCallback = callback;
	}
}

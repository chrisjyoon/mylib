package com.chrisjyoon.camera;

import android.app.Activity;
import android.hardware.Camera;
import android.hardware.Camera.CameraInfo;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.Surface;
import android.view.View;
import android.view.ViewGroup;

import com.chrisjyoon.common.CommonUtil;
import com.chrisjyoon.common.Constants;
import com.chrisjyoon.common.Logger;

public class CameraFragment extends Fragment {
	private Camera mCamera = null;
	private CameraPreview mCameraView = null;
	private CameraInterface mCameraCallback;
    // The first rear facing camera
    int defaultCameraId;
    
    public static CameraFragment newInstance(int photoW, int photoH, int cropW, int cropH) {
    	CameraFragment cf = new CameraFragment();
    	Bundle args = new Bundle();
    	args.putInt(Constants.KEY_PARAM_PHOTO_WIDTH, photoW);
    	args.putInt(Constants.KEY_PARAM_PHOTO_HEIGHT, photoH);
    	args.putInt(Constants.KEY_PARAM_CROP_WIDTH, cropW);
    	args.putInt(Constants.KEY_PARAM_CROP_HEIGHT, cropH);
    	cf.setArguments(args);
    	return cf;
    }
    
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		mCameraView = new CameraPreview(getActivity());
		mCameraView.setCameraCallback(mCameraCallback);
		
		mCameraView.setPhotoSize(getArguments().getInt(Constants.KEY_PARAM_PHOTO_WIDTH), 
				getArguments().getInt(Constants.KEY_PARAM_PHOTO_HEIGHT));
		mCameraView.setCropSize(getArguments().getInt(Constants.KEY_PARAM_CROP_WIDTH), 
				getArguments().getInt(Constants.KEY_PARAM_CROP_HEIGHT));
		
		// Find the total number of cameras available
        int numberOfCameras = Camera.getNumberOfCameras();
        // Find the ID of the default camera
        CameraInfo cameraInfo = new CameraInfo();
        for (int i = 0; i < numberOfCameras; i++) {
            Camera.getCameraInfo(i, cameraInfo);
            
            if (cameraInfo.facing == CameraInfo.CAMERA_FACING_BACK) {
                defaultCameraId = i;
            }
        }
		
	    return mCameraView;
	}
	
	@Override
	public void onResume() {
		super.onResume();
		try {
			mCamera = Camera.open();
			
			mCameraView.setCamera(mCamera);
	        setDisplayOrientation();
		} catch (Exception e) {
			mCameraCallback.cameraFail();
		}
 	}
	
	@Override
	public void onPause() {
		super.onPause();
		if (mCamera != null) {
            mCameraView.setCamera(null);
            mCamera.release();
            mCamera = null;
        }
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		mCameraCallback = (CameraInterface) activity;
	}

	public void autoFocus() {
		mCameraView.autoFocus();
	}

    public void takePicture() {
        mCameraView.takePicture();
    }

	public void setDisplayOrientation() {
		int displayOrientation = -1;
        int rotation = getActivity().getWindowManager().getDefaultDisplay().getRotation();
        Camera.CameraInfo info=new Camera.CameraInfo();
        int degrees = 0;
        DisplayMetrics dm = new DisplayMetrics();
        
        Camera.getCameraInfo(defaultCameraId, info);
        
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);
        Logger.d("rotation = " + rotation);
        switch (rotation) {
        	case Surface.ROTATION_0:
        		degrees=0;
        		break;
        	case Surface.ROTATION_90:
        		degrees=90;
        		break;
        	case Surface.ROTATION_180:
        		degrees=180;
        		break;
        	case Surface.ROTATION_270:
        		degrees=270;
        		break;
        }
        if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
        	displayOrientation=(info.orientation + degrees) % 360;
        	displayOrientation=(360 - displayOrientation) % 360;
        } else {
        	displayOrientation=(info.orientation - degrees + 360) % 360;
        }
        if (!CommonUtil.isEmulator()) {
        	mCameraView.setDisplayOrientation(displayOrientation);
        }
	}
}
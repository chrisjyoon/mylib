package com.chrisjyoon.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.chrisjyoon.R;

/**
 *	마지막 텍스트를 hint 로 사용한다. 	 
 *
 */
public class SpinnerHintAdapter extends ArrayAdapter<String> {

	public SpinnerHintAdapter(Context context, int resource) {
		super(context, resource);
	}

	@Override
	public int getCount() {
		return (super.getCount() - 1);
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View v = super.getView(position, convertView, parent);
		if (position == getCount()) {
			((TextView) v.findViewById(R.id.tvSpinner)).setText("");
			((TextView) v.findViewById(R.id.tvSpinner)).setHint(getItem(getCount()));
		}
		
		return v;
	}
}

package com.chrisjyoon.location;

import android.location.Location;
import android.os.Bundle;

import com.chrisjyoon.BaseActivity;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;

public class MapActivity extends BaseActivity implements LocationListener,
	GooglePlayServicesClient.ConnectionCallbacks,
	GooglePlayServicesClient.OnConnectionFailedListener {

	protected GoogleApiClient mGoogleApiClient;
	// A request to connect to Location Services
    protected LocationRequest mLocationRequest;
    // Stores the current instantiation of the location client in this object

    protected GoogleMap mMap;
    
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		mGoogleApiClient = new GoogleApiClient.Builder(this)
        .addApi(LocationServices.API)
        .addConnectionCallbacks(mConnectionCallbacks)
        .addOnConnectionFailedListener(mOnConnectionFailedListener)
        .build();
		
		// Create a new global location parameters object
        mLocationRequest = LocationRequest.create().setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        
        /*
         * Set the update interval
         */
        mLocationRequest.setInterval(LocationUtils.UPDATE_INTERVAL_IN_MILLISECONDS);
        
        PendingResult<Status> result = LocationServices.FusedLocationApi
        	    .requestLocationUpdates(
        	        mGoogleApiClient,   // your connected GoogleApiClient
        	        mLocationRequest,   // a request to receive a new location
        	        this); // the listener which will receive updated locations

        
	}
	
	private GoogleApiClient.ConnectionCallbacks mConnectionCallbacks = new GoogleApiClient.ConnectionCallbacks() {

		@Override
		public void onConnected(Bundle connectionHint) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onConnectionSuspended(int cause) {
			// TODO Auto-generated method stub
			
		}
		
	};
	
	private GoogleApiClient.OnConnectionFailedListener mOnConnectionFailedListener = new GoogleApiClient.OnConnectionFailedListener() {

		@Override
		public void onConnectionFailed(ConnectionResult result) {
			// TODO Auto-generated method stub
			
		}
	};
	
	@Override
	public void onConnectionFailed(ConnectionResult arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onConnected(Bundle arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onDisconnected() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onLocationChanged(Location location) {
		// TODO Auto-generated method stub
		
	}
	
	public void setUpMapIfNeeded(int idMap, double lat, double lon, float zoom) {
	    // Do a null check to confirm
		if (mMap == null) {
			mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(idMap)).getMap();
			
			if (mMap != null) {
				moveMap(lat, lon, zoom);
				
				mMap.setMyLocationEnabled(true);
				
//				GoogleMapOptions options = new GoogleMapOptions();
//				options
			}
		}
	}
	
	public void moveMap(double lat, double lon, float zoom) {
		LatLng llNew = new LatLng(lat, lon);
		mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(llNew, zoom));
	}
}
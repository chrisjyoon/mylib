package com.chrisjyoon.image;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.TransitionDrawable;
import android.util.AttributeSet;

import com.android.volley.toolbox.NetworkImageView;

public class FadeInNetworkImageView extends NetworkImageView {

	private static final int FADE_IN_TIME_MS = 500;
	
	public FadeInNetworkImageView(Context context) {
		super(context);
	}
	
	public FadeInNetworkImageView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	@Override
	public void setImageBitmap(Bitmap bm) {
		TransitionDrawable td = new TransitionDrawable(new Drawable[] {
			new ColorDrawable(android.R.color.transparent),
			new BitmapDrawable(getResources(), bm)
		});
		
		setImageDrawable(td);
		td.setCrossFadeEnabled(true);
		td.startTransition(FADE_IN_TIME_MS);

//		super.setImageBitmap(bm);
	}
	
	
}

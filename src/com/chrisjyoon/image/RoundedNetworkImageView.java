package com.chrisjyoon.image;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.AttributeSet;

import com.android.volley.toolbox.NetworkImageView;

public class RoundedNetworkImageView extends NetworkImageView {


	public RoundedNetworkImageView(Context context) {
		super(context);
	}

	public RoundedNetworkImageView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	@Override
	public void setImageBitmap(Bitmap bm) {
        if (bm == null) return;
        RoundedPhotoDrawable rd = new RoundedPhotoDrawable(bm);
		setImageDrawable(rd);
	}
	
	
}

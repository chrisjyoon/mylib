package com.chrisjyoon;

import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;

public class SlideMenuActivity extends BaseActivity {

	protected DrawerLayout mDrawerLayout;
	protected ActionBarDrawerToggle mDrawerToggle = null;

	private FrameLayout mLayoutBody;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
	}

	public void initLayout(int rootLayout) {
		if (rootLayout != 0) {
			super.setContentView(rootLayout);
		} else {
            super.setContentView(R.layout.activity_slide_menu);
		}
		
		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		
		if (mDrawerLayout != null) {
	        mDrawerToggle = new ActionBarDrawerToggle(
	                this,                  /* host Activity */
	                mDrawerLayout,         /* DrawerLayout object */
	                R.drawable.ic_drawer,  /* nav drawer image to replace 'Up' caret */
	                R.string.drawer_open,  /* "open drawer" description for accessibility */
	                R.string.drawer_close  /* "close drawer" description for accessibility */
	                ) {
	            public void onDrawerClosed(View view) {
	//            	getSupportActionBar().setTitle("test");
	//                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
	            }
	
	            public void onDrawerOpened(View drawerView) {
	//            	getSupportActionBar().setTitle("test2");
	//                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
	            }
	        };
	        
	        mDrawerLayout.setDrawerListener(mDrawerToggle);
		}
	}
	
	@Override
	public void setContentView(int layoutResID) {
		mLayoutBody = (FrameLayout) findViewById(R.id.content_frame);
		getLayoutInflater().inflate(layoutResID, mLayoutBody);
	}

	@Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        
        if (mDrawerToggle != null) {
	        // Sync the toggle state after onRestoreInstanceState has occurred.
	        mDrawerToggle.syncState();
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggls
        if (mDrawerToggle != null) {
    	    mDrawerToggle.onConfigurationChanged(newConfig);
        }
    }

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (mDrawerToggle != null && mDrawerToggle.onOptionsItemSelected(item)) {
	          return true;
	    }
		
		return super.onOptionsItemSelected(item);
	}
    
	public int getContentFrameId() {
		return R.id.content_frame;
	}
    
}

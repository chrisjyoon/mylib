package com.chrisjyoon;

import android.support.v7.app.ActionBarActivity;

import com.chrisjyoon.common.Logger;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;

public class BaseActivity extends ActionBarActivity {
    private final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
	
	@Override
	public void setContentView(int layoutResID) {
		super.setContentView(layoutResID);
	}

    /**
     * Check the device to make sure it has the Google Play Services APK. If
     * it doesn't, display a dialog that allows users to download the APK from
     * the Google Play Store or enable it in the device's system settings.
     */
    public boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, this,
                        PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                Logger.i("This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }
}

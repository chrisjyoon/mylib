package com.chrisjyoon.file;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileFilter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Date;
import java.util.Locale;

import android.os.Environment;

import com.chrisjyoon.common.Logger;

public class FileUtil {
	public static final int MEDIA_TYPE_IMAGE = 1;
	public static final int MEDIA_TYPE_VIDEO = 2;

	
	public static void saveFile(File f, ByteArrayOutputStream baos) 
			throws FileNotFoundException, IOException {
		byte[] data = baos.toByteArray();
		
		saveFile(f, data);
	}
	
	public static void saveFile(File f, byte[] data) 
			throws FileNotFoundException, IOException {
		FileOutputStream fos = new FileOutputStream(f.getPath());
		fos.write(data);
		fos.getFD().sync();
		fos.close();
	}
	
	public static File[] getListInOrder(File dir) {
		File[] listFiles = null;
	
		if (dir.exists()) {
			FileFilter filter = new FileFilter() {
				@Override
				public boolean accept(File file) {
					return file.isFile();
				}
			};
			
			
			listFiles = dir.listFiles(filter);
			
			Arrays.sort(listFiles, new Comparator<File>() {
				@Override
				public int compare(File lhs, File rhs) {
					return lhs.getName().compareTo(rhs.getName());
				}
			});
		}
		
		return listFiles;
	}
	
	public static File getOuputMediaDir(String dir) {
		File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
            Environment.DIRECTORY_PICTURES), dir);
		// This location works best if you want the created images to be shared
		// between applications and persist after your app has been uninstalled.
		
		// Create the storage directory if it does not exist
		if (! mediaStorageDir.exists()){
			if (! mediaStorageDir.mkdirs()){
				Logger.d("failed to create directory");
				return null;
			}
		}
		return mediaStorageDir;
	}
	
	/** Create a File for saving an image or video */
	public static File getOutputMediaFile(int type, String dir, String filePrefix){
	    // To be safe, you should check that the SDCard is mounted
	    // using Environment.getExternalStorageState() before doing this.

	    File mediaStorageDir = getOuputMediaDir(dir);
	    if (mediaStorageDir == null) {
	    	return null;
	    }
	    
	    Logger.d("make new file");
	    // Create a media file name
	    String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.KOREA).format(new Date());
	    File mediaFile;
	    if (type == MEDIA_TYPE_IMAGE){
	        mediaFile = new File(mediaStorageDir.getPath() + File.separator +
	        filePrefix + "IMG_"+ timeStamp + ".jpg");
	    } else if(type == MEDIA_TYPE_VIDEO) {
	        mediaFile = new File(mediaStorageDir.getPath() + File.separator +
	        filePrefix + "VID_"+ timeStamp + ".mp4");
	    } else {
	        return null;
	    }
	    
	    return mediaFile;
	}
	
	public static File getOutputMediaFileWithName(int type, String dir, String fileName) {
		File mediaStorageDir = getOuputMediaDir(dir);
	    if (mediaStorageDir == null) {
	    	return null;
	    }
	    
	    File mediaFile;
	    if (type == MEDIA_TYPE_IMAGE){
	        mediaFile = new File(mediaStorageDir.getPath() + File.separator + fileName);
	    } else if(type == MEDIA_TYPE_VIDEO) {
	        mediaFile = new File(mediaStorageDir.getPath() + File.separator + fileName);
	    } else {
	        return null;
	    }
	    
	    return mediaFile;
	}
}
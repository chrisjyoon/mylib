package com.chrisjyoon.common;
import android.app.Dialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.util.DisplayMetrics;
import android.util.TypedValue;

import com.google.android.gms.common.GooglePlayServicesUtil;

public class CommonUtil {

	public static int servicesConnected(Context ctx) {
		// Check that Google Play services is available
		int resultCode =
				GooglePlayServicesUtil.
                isGooglePlayServicesAvailable(ctx);
		
		return resultCode;
    }

	/** Check if this device has a camera */
	public static boolean checkCameraHardware(Context ctx) {
	    if (ctx.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)){
	        // this device has a camera
	        return true;
	    } else {
	        // no camera on this device
	        return false;
	    }
	}
	
	/**
	 * 에뮬레이터 실행 여부 판단
	 * @return true : 에뮬레이터, false : 폰
	 */
	public static boolean isEmulator() {
		if ("vbox86p".equals(Build.PRODUCT) || "google_sdk".equals(Build.PRODUCT)) {
			return true;
		}
		return false;
	}

	public static Dialog getErrorDialog(Context context, String errMsg) {
		Dialog errDlg = new Dialog(context);
		errDlg.setTitle(errMsg);
		
		return errDlg;
	}
	
	public static int getActionBarHeight(Context context){
        int actionBarHeight = 0;
        TypedValue tv = new TypedValue();
        if (context.getTheme().resolveAttribute(android.R.attr.actionBarSize, tv, true)) {
            actionBarHeight = TypedValue.complexToDimensionPixelSize(tv.data, context.getResources().getDisplayMetrics());
        }
        return actionBarHeight;
    }
	
	public static int getDeviceWidth(Context context) {
		DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
		return displayMetrics.widthPixels;
	}

	public static int getDeviceHeight(Context context) {
		DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
		return displayMetrics.heightPixels;
	}
}

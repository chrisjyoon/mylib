package com.chrisjyoon.common;

public class Constants {
	public static final String KEY_PARAM_PHOTO_WIDTH = "photo_width";
	public static final String KEY_PARAM_PHOTO_HEIGHT = "photo_height";
	public static final String KEY_PARAM_CROP_WIDTH = "crop_width";
	public static final String KEY_PARAM_CROP_HEIGHT = "crop_height";
}

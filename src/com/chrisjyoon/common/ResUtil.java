package com.chrisjyoon.common;

import java.io.InputStream;

import android.content.Context;
import android.content.res.AssetManager;
import android.text.TextUtils;

public class ResUtil {
	public static String readAsset(Context ctx, String fileName) {
		AssetManager assetMgr = ctx.getResources().getAssets();
		InputStream is = null;
		String result = null;
		try {
			is = assetMgr.open(fileName);
			int size = is.available();
			
			if (size > 0) {
				byte[] data = new byte[size];
				is.read(data);
				result = new String(data);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (is != null) {
				try {
					is.close();
					is = null;
				} catch (Exception e) {}
			}
		}
		assetMgr = null;
		return result;
	}
	
	public static int getDrawableResourceID(Context ctx, String resName) {
		if (TextUtils.isEmpty(resName)) {
			return -1;
		}
		return ctx.getResources().getIdentifier(resName, "drawable", ctx.getPackageName());
	}
}

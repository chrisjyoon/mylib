package com.chrisjyoon.common;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.KeyEvent;

import com.chrisjyoon.R;

public class AlertDialogFragment extends DialogFragment {
	public static final int TYPE_OK = 0;
	public static final int TYPE_OK_CANCEL = 1;

    private static final String TITLE = "title";
    private static final String MESG = "mesg";
    private static final String TYPE = "type";

	private OnDialogButtonClick mClickListener = null;
	
	public interface OnDialogButtonClick {
		public void doPositiveClick(String tag);
		public void doNegativeClick(String tag);
        public void doBackKeyClick(String tag);
	}
	
	public static AlertDialogFragment newInstance(int title, int mesg, int type) {
        AlertDialogFragment frag = new AlertDialogFragment();
        Bundle args = new Bundle();
        args.putInt(TITLE, title);
        args.putInt(MESG, mesg);
        args.putInt(TYPE, type);
        frag.setArguments(args);
        
        return frag;
	}

    public static AlertDialogFragment newInstance(int title, String mesg, int type) {
        AlertDialogFragment frag = new AlertDialogFragment();
        Bundle args = new Bundle();
        args.putInt(TITLE, title);
        args.putString(MESG, mesg);
        args.putInt(TYPE, type);
        frag.setArguments(args);

        return frag;
    }

	@Override @NonNull
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		int title = getArguments().getInt(TITLE);
		int mesgId = getArguments().getInt(MESG);
		int type = getArguments().getInt(TYPE);

        if (mesgId == 0) {
            String mesg = getArguments().getString(MESG);
            if (type == TYPE_OK) {
                return AlertOKDialog(title, mesg);
            } else {
                return AlertOKCancelDialog(title, mesg);
            }
        } else {
            if (type == TYPE_OK) {
                return AlertOKDialog(title, mesgId);
            } else {
                return AlertOKCancelDialog(title, mesgId);
            }
        }
	}
	
	public void setOnDialogButtonClick(OnDialogButtonClick listener) {
		mClickListener = listener;
	}

    public Dialog AlertOKDialog(int title, int mesg) {
        return AlertOKDialog(title, getString(mesg));
    }
    public Dialog AlertOKDialog(int title, String mesg) {
        return new AlertDialog.Builder(getActivity())
//			.setIcon(R.drawable.alert_dialog_icon)
            .setTitle(title)
            .setMessage(mesg)
            .setPositiveButton(R.string.alert_dialog_ok,
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            if (getTag() != null) {
                                if (mClickListener == null) {
                                    doPositiveClick();
                                } else {
                                    mClickListener.doPositiveClick(getTag());
                                }
                            }
                        }
                    })
            .setOnKeyListener(new DialogInterface.OnKeyListener() {
                @Override
                public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                    if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_UP) {
                        if (mClickListener != null) {
                            mClickListener.doBackKeyClick(getTag());
                        } else {
                            doBackKeyClick();
                        }
                    }
                    return false;
                }
            })
            .create();
    }

    public Dialog AlertOKCancelDialog(int title, int mesg) {
        return AlertOKCancelDialog(title, getString(mesg));
    }
	public Dialog AlertOKCancelDialog(int title, String mesg) {
		return new AlertDialog.Builder(getActivity())
//			.setIcon(R.drawable.alert_dialog_icon)
			.setTitle(title)
			.setMessage(mesg)
			.setPositiveButton(R.string.alert_dialog_ok,
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            if (mClickListener == null) {
                                doPositiveClick();
                            } else {
                                mClickListener.doPositiveClick(getTag());
                            }
                        }
                    })
			.setNegativeButton(R.string.alert_dialog_cancel,
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            if (mClickListener == null) {
                                doNegativeClick();
                            } else {
                                mClickListener.doNegativeClick(getTag());
                            }
                        }
                    })
            .setOnKeyListener(new DialogInterface.OnKeyListener() {
                @Override
                public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                    if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_UP) {
                        if (mClickListener != null) {
                            mClickListener.doBackKeyClick(getTag());
                        } else {
                            doBackKeyClick();
                        }
                    }
                    return false;
                }
            })
            .create();
	}
	
	public void doPositiveClick() {
		if (getActivity() instanceof OnDialogButtonClick) {
			((OnDialogButtonClick) getActivity()).doPositiveClick(getTag());
		}
	}
	
	public void doNegativeClick() {
		if (getActivity() instanceof OnDialogButtonClick) {
			((OnDialogButtonClick) getActivity()).doNegativeClick(getTag());
		}
	}

    public void doBackKeyClick() {
        if (getActivity() instanceof OnDialogButtonClick) {
            ((OnDialogButtonClick) getActivity()).doBackKeyClick(getTag());
        }
    }
}

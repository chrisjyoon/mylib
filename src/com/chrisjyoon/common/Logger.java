package com.chrisjyoon.common;

import android.util.Log;

public class Logger {
	private static final String TAG = "chris";
	
	public static void d(String mesg) {
		Log.d(TAG, mesg);
	}
	
	public static void d(String tag, String mesg) {
		Log.d(tag, mesg);
	}
	
	public static void i(String mesg) {
		Log.i(TAG, mesg);
	}
	
	public static void e(String errMesg) {
		Log.e(TAG, errMesg);
	}
	
	public static void e(String mesg, Exception e) {
		Log.e(TAG, mesg, e);
	}
	
	public static void e(String tag, String mesg, Exception e) {
		Log.e(tag, mesg, e);
	}
	
	public static void e(String tag, String errMesg) {
		Log.e(tag, errMesg);
	}
}

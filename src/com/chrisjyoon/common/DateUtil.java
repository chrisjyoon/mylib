package com.chrisjyoon.common;

import java.util.Calendar;
import java.util.Locale;
import java.util.TimeZone;

public class DateUtil {
	
	public static String getTodayStr() {
		Calendar now = Calendar.getInstance();
		
		return String.format(Locale.KOREA, "%4d%02d%02d", now.get(Calendar.YEAR), 
				now.get(Calendar.MONTH) + 1, 
				now.get(Calendar.DAY_OF_MONTH));
	}
	
	public static long diffNow(Calendar cal) {
		Calendar now = Calendar.getInstance();
		
		return (now.getTimeInMillis() - cal.getTimeInMillis());
	}
	
	public static long diffNowGMT(String year, String mon, String day, String hour, String min, String sec) {
		Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
		
		long now = cal.getTimeInMillis();
		
		cal.set(Integer.parseInt(year), 
				Integer.parseInt(mon) - 1, 
				Integer.parseInt(day), 
				Integer.parseInt(hour), 
				Integer.parseInt(min), 
				Integer.parseInt(sec));
		
		return (now - cal.getTimeInMillis());
	}
}
